import QtQuick 2.0
import "gameboard"

Rectangle {
    id: rectangle1

    Column {
        id: column1
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        spacing: 10

        Menu    {
            id: menu1;
            height: 40;
            menuBtn1.caption: "Start"
            menuBtn1.onButtonClicked: {
                if(rectangle1.state == "Screen_2")
                {
                    board1.stop();
                    rectangle1.state = "Screen_1"
                }
                else
                {
                    board1.start();
                    rectangle1.state = "Screen_2"
                }
            }

            menuBtn2.caption: "Load"
            menuBtn2.onButtonClicked: {
                if(rectangle1.state == "Screen_2")
                    board1.save()
                else
                {
                    board1.load()
                    rectangle1.state = "Screen_3"
                }
            }

            menuBtn3.visible: false
            menuBtn3.caption: "Settings"
            menuBtn3.onButtonClicked: {
                rectangle1.state = "Screen_4"
            }
        }
        Menu    {
            id: menu2;
            height: 25;
            menuBtn1.caption: "Prev <<"
            menuBtn1.onButtonClicked: {
                board1.prevLoadedMove()
            }

            menuBtn2.caption: ">> Next"
            menuBtn2.onButtonClicked: {
                board1.nextLoadedMove()
            }

            menuBtn3.visible: false
            menuBtn4.visible: false
        }

        Board   { id: board1; dimensions: 8; borders: 5; }
    }

    states: [
        State {
            name: "Screen_1"

            PropertyChanges {
                target: board1
                enabled: false
            }

            PropertyChanges {
                target: menu2
                visible: false
            }
        },
        State {
            name: "Screen_2"

            PropertyChanges {
                target: menu1
                menuBtn1.caption: "Stop"
                menuBtn2.caption: "Save"
            }

            PropertyChanges {
                target: menu2
                visible: false
            }
        },
        State {
            name: "Screen_3"

            PropertyChanges {
                target: board1
                enabled: false
            }
        },
        State {
            name: "Screen_4"
            PropertyChanges {
                target: board1
                visible: false
                enabled: false
            }

            PropertyChanges {
                target: menu2
                visible: false
            }
        }
    ]
    state: "Screen_1"
}

