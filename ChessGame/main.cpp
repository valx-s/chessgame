#include <QApplication>
#include <QQmlApplicationEngine>
#include <src/gamecontroller.h>
#include <src/gamecell.h>
#include <src/gamefigure.h>
#include <src/gamearmy.h>
#include <src/gamemove.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<Game>("Controller", 1, 0, "Game");
    qmlRegisterType<Cell>("Controller", 1, 0, "Cell");
    qmlRegisterType<Figure>("Controller", 1, 0, "Figure");
    qmlRegisterType<Army>("Controller", 1, 0, "Army");
    qmlRegisterType<Move>("Controller", 1, 0, "Move");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
