#include "gamefigure.h"
#include "gamecellhandler.h"
//----------------------------------------------------------
Figure::Figure() : Item(), moved(0), isPrimary(false), pos(""), lostTurn(0)
{
    hide();
}
//----------------------------------------------------------
Figure::~Figure()
{

}
//----------------------------------------------------------
QQmlListProperty<Move> Figure::moves() const
{
    return QQmlListProperty<Move>((QObject *)this, 0, &Figure::append_move, NULL, NULL, NULL);
}
//----------------------------------------------------------
void Figure::append_move(QQmlListProperty<Move> *list, Move *item)
{
    Figure *parent = qobject_cast<Figure *>(list->object);
    if (parent)
        parent->m_moves.append(item);
}
//----------------------------------------------------------
const QString Figure::toString() const
{
    return QString("%1 Figure: %2 %3 %4 %5 %6 %7").arg(Item::toString(), pos, property("image").toString())
            .arg(isPrimary).arg(lostTurn).arg(moved).arg(m_moves.size());
}
//----------------------------------------------------------
void Figure::hide()
{
    Item::hide();
    Cell *cell = CellsUtil.getCell(getPos());
    if(cell)
        cell->setOccupied(Army::NONE);
}

//----------------------------------------------------------
bool Figure::canMove(const Item *item)
{
    Positions moves = getAvailableMoves();
    for(Positions::iterator i = moves.begin(); i != moves.end(); ++i)
        if(item->at(*i))
            return true;
    return false;
}

//----------------------------------------------------------
Positions Figure::getAvailableMoves() const
{
    Positions moves;
    for(Moves::ConstIterator i = m_moves.begin(); i != m_moves.end(); ++i)
    {
        Move *move = *i;
        if(move->getAllowed())
            moves += move->moves(getPos());
        else
        {
            Positions pos = move->moves(getPos());
            for(Positions::iterator j = pos.begin(); j != pos.end(); ++j)
                moves.removeAll((*j));
        }
    }
    return moves;
}
//----------------------------------------------------------
void Figure::onSelect()
{
    setZ(z() + 1);

    CellsUtil.highlightAvailable(getAvailableMoves());
}
//----------------------------------------------------------
void Figure::onUnselect()
{
    setZ(z() - 1);

    CellsUtil.stopHighlight();
}
//----------------------------------------------------------
void Figure::onMove()
{
    if(moveAllowed(this, getTop(), getLeft()))
        setMovedTimes(movedTimes() + 1);
}
//----------------------------------------------------------
void Figure::oppositeMoves()
{
    for(Moves::Iterator i = m_moves.begin(); i != m_moves.end(); ++i)
        (*i)->setDirection(::getOpposite((*i)->getDirection()));
}

//----------------------------------------------------------
void Figure::rightMoves()
{
    for(Moves::ConstIterator i = m_moves.begin(); i != m_moves.end(); ++i)
        (*i)->setDirection(::getRight((*i)->getDirection()));
}

//----------------------------------------------------------
void Figure::leftMoves()
{
    for(Moves::ConstIterator i = m_moves.begin(); i != m_moves.end(); ++i)
        (*i)->setDirection(::getLeft((*i)->getDirection()));
}

//----------------------------------------------------------
