#include "gamecontroller.h"
#include <gameitem.h>
#include <gamecellhandler.h>
//----------------------------------------------------------
Game::Game() : rowCount(0), colCount(0), curSide(Army::NONE)
{
    qDebug(__FUNCTION__);
    connect(&turns, SIGNAL(startTurn(Side)), this, SLOT(onStartTurn(Side)));
}
//----------------------------------------------------------
Game::~Game()
{
    qDebug(__FUNCTION__);
}
//----------------------------------------------------------
QQmlListProperty<Army> Game::armies() const
{
    return QQmlListProperty<Army>((QObject *)this, 0, &Game::append_army, NULL, NULL, NULL);
}
//----------------------------------------------------------
void Game::append_army(QQmlListProperty<Army> *list, Army *item)
{
    Game *parent = qobject_cast<Game *>(list->object);
    if (parent)
    {
        parent->m_armies.append(item);
        connect(item, SIGNAL(error(QString)), parent, SIGNAL(error(QString)));
        connect(item, SIGNAL(warning(QString)), parent, SIGNAL(warning(QString)));
        parent->turns.addSide(item->getSide());

        ArmyFigures figures = item->getArmyFigures();
        for(ArmyFigures::iterator i = figures.begin(); i != figures.end(); ++i)
            connect(*i, SIGNAL(moveAllowed(Figure*,uint,uint)), parent, SLOT(figureMove(Figure*,uint,uint)));
    }
}
//----------------------------------------------------------
void Game::start()
{
    turns.reset();
    movesLog.clear();

    CellsUtil.setCells(getCells());
    CellsUtil.prepareCells(cols(), rows());

    for(Armies::Iterator a = m_armies.begin(); a != m_armies.end(); ++a)
    {
        Army *army = *a;
        army->placeFigures();
//        ArmyFigures figures = army->getArmyFigures();
//        for(ArmyFigures::iterator i = figures.begin(); i != figures.end(); ++i)
//            connect(*i, SIGNAL(moveAllowed(Figure*,uint,uint)), this, SLOT(figureMove(Figure*,uint,uint)));
    }
    turns.nextTurn();
    emit gameStarted();
}
//----------------------------------------------------------
Army *Game::getArmy(Army::ArmySide side)
{
    for(Armies::Iterator i = m_armies.begin(); i != m_armies.end(); ++i)
        if((*i)->getSide() == side)
            return *i;
    return NULL;
}
//----------------------------------------------------------
bool Game::figureMove(Figure *sender, const unsigned x, const unsigned y)
{
    Cell *from = CellsUtil.getCell(sender->getPos());
    Cell *to = CellsUtil.getCellByCoords(x, y, sender->width(), sender->height());
    if(from && to && sender->canMove(to))
    {
        movesLog.appendMove(from->getPos().asString(), to->getPos().asString());
        return figureMove(sender, from, to);
    }
    else if(from)
        from->place(sender);
    qDebug() << __FUNCTION__ << "unknown current cell: " << sender->toString();
    return false;
}
//----------------------------------------------------------
bool Game::figureMove(Figure *sender, Cell *from, Cell *to)
{
    if(to && from && sender && to != from)
    {
        qDebug() << turns.getTurn() << __FUNCTION__ << sender->toString() << "FROM:" << from->toString() << "TO:" << to->toString();
        if(to->occupiedBy() != Army::NONE)
        {
            Army *enemy = getArmy(to->occupiedBy());
            if(enemy)
            {
                enemy->lostFigure(to, turns.getTurn());
//                if(sender->primary())
//                {
//                    warning(::toString(enemy->getSide()) + "\nLost primary figure !");
//                    stop();
//                }
            }
//            else
//                qDebug() << "unknown enemy spoted !" << to->toString();
        }
        from->setOccupied(Army::NONE);
        to->place(sender);
        to->setOccupied(curSide);

        if(turns.getDirection() == FORWARD)
        {
            turns.nextTurn();
            checkPrimary();
        }
        else
        {
//            qDebug() << "resurecting" << ::toString(curSide);
            for(Armies::Iterator i = m_armies.begin(); i != m_armies.end(); ++i)
            {
                if((*i)->getSide() != curSide)
                    (*i)->resurectFigure(turns.getTurn());
            }
        }
        return true;
    }
    else if(from && sender)
    {
        from->place(sender);
//        qDebug() << "target cell forbidden or not exist";
    }
    return false;
}
//----------------------------------------------------------
void Game::setCurrentSide(Army::ArmySide value)
{
    Army *currentArmy = getArmy(curSide);
    if(currentArmy)
        currentArmy->enableFigures(false);
    for(Armies::Iterator i = m_armies.begin(); i != m_armies.end(); ++i)
    {
        Army *army = *i;
        if(army->getSide() == value)
        {
            if(currentArmy != army)
            {
                currentArmy = army;
                curSide = value;
                currentSideChanged();
            }
            currentArmy->enableFigures(true);
            break;
        }
    }
}

//----------------------------------------------------------
void Game::stop()
{
    emit gameStopped();

    for(Armies::Iterator a = m_armies.begin(); a != m_armies.end(); ++a)
    {
        ArmyFigures figures = (*a)->getArmyFigures();
        for(ArmyFigures::iterator i = figures.begin(); i != figures.end(); ++i)
        {
            Figure *figure = *i;
            figure->hide();
//            disconnect(figure, SIGNAL(moveAllowed(Figure*,uint,uint)), this, SLOT(figureMove(Figure*,uint,uint)));
        }
        (*a)->enableFigures(false);
    }
}
//----------------------------------------------------------
void Game::checkPrimary()
{
//    Army *army = getArmy(curSide);
//    if(army)
//    {
//        ArmyFigures figures = army->getArmyFigures();
//        for(ArmyFigures::iterator i = figures.begin(); i != figures.end(); ++i)
//        {
//            // todo: check
//            if((*i)->primary() && !(*i)->isVisible())
//            {
//                warning(::toString(curSide) + "\nLost primary figure !");
//                stop();
//            }
//        }
//    }
}

//----------------------------------------------------------
void Game::load()
{
    start();

    if(movesLog.load())
    {
    //  autoload last move
//        const unsigned turns = movesLog.movesCount();
//        for(unsigned i = 0; i < turns; ++i)
//            nextLoadedMove();
    }
}

//----------------------------------------------------------
void Game::save()
{
    movesLog.save();
}
//----------------------------------------------------------
void Game::prevLoadedMove()
{
    QString from_pos;
    QString to_pos;
    turns.setDirection(BACKWARD);
    turns.nextTurn();
    if(movesLog.move(turns.getTurn() - 1, from_pos, to_pos))
    {
        Cell *from = CellsUtil.getCell(Pos(to_pos));
        Cell *to = CellsUtil.getCell(Pos(from_pos));
        Figure *figure = getArmy(curSide)->getFigureAt(from);

        figureMove(figure, from, to);
    }
}

//----------------------------------------------------------
void Game::nextLoadedMove()
{
    QString from_pos;
    QString to_pos;
    turns.setDirection(FORWARD);
    if(movesLog.move(turns.getTurn() - 1, from_pos, to_pos))
    {
        Cell *from = CellsUtil.getCell(Pos(from_pos));
        Cell *to = CellsUtil.getCell(Pos(to_pos));
        Figure *figure = getArmy(curSide)->getFigureAt(from);

        figureMove(figure, from, to);
    }
}

//----------------------------------------------------------
