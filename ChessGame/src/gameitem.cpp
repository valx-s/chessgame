#include "gameitem.h"
#include <QLineF>
//----------------------------------------------------------
unsigned strToIntPos(const QString &value)
{
    static const QString dict = "abcdefghijklmnopqrstuvwxyz";

    unsigned number = 0;
    int mul = 0;
    while(mul < value.size())
    {
        number += dict.indexOf(value[mul]) + 10 * mul + 1;
        mul++;
    }
//        number += dict.indexOf(value[mul]) * (++mul) + 1;
    return number;
}
//----------------------------------------------------------
Pos::Pos(const QString &pos)
{
    QRegExp parts("([a-z]+)([0-9]+)");
    if(parts.indexIn(pos.toLower()) > -1)
    {
        x = strToIntPos(parts.cap(1));
        y = parts.cap(2).toInt();
    }
    else if(pos.split(":").size() > 1)
    {
        x = pos.split(":")[0].toInt();
        y = pos.split(":")[1].toInt();
    }
    else
    {
        x = 0;
        y = 0;
    }
}
//----------------------------------------------------------
QString Pos::asString() const
{
    return QString("%1:%2").arg(x).arg(y);
}
//----------------------------------------------------------
Item::Item()
{

}
//----------------------------------------------------------
Item::~Item()
{

}
//----------------------------------------------------------
const QString Item::toString() const
{
    return QString("%1 %2,%3").arg("Item").arg(position.x).arg(position.y);
}
//----------------------------------------------------------
void Item::setPos(const QString &pos)
{
    const Pos p(pos);
    if(p.x > 0 && p.y > 0)
        setPos(p.x, p.y);
    else
        qDebug() << "invalid position format";
}
//----------------------------------------------------------
bool Item::at(unsigned x, unsigned y) const
{
    return position.x == x && position.y == y;
}
//----------------------------------------------------------
bool Item::at(const QString &pos) const
{
    return position == Pos(pos);
}
//----------------------------------------------------------
qreal Item::distance(unsigned x, unsigned y, unsigned x_range, unsigned y_range)
{
    return QLineF(getTop() + height() / 2, getLeft() + width() / 2, x + x_range / 2, y + y_range / 2).length();
}

//----------------------------------------------------------
