#ifndef GAMEFIGURE_H
#define GAMEFIGURE_H

#include <QObject>
#include <gameitem.h>
#include <gamemove.h>

typedef QList<Move *> Moves;

class Figure : public Item
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Move> moves READ moves)
    static void append_move(QQmlListProperty<Move> *list, Move *item);
    Moves m_moves;

    Q_PROPERTY(unsigned movedTimes READ movedTimes WRITE setMovedTimes NOTIFY movedTimesChanged)
    unsigned moved;
    Q_PROPERTY(bool primary READ primary WRITE setPrimary NOTIFY primaryChanged)
    bool isPrimary;
    Q_PROPERTY(QString position READ position WRITE setPosition NOTIFY positionChanged)
    QString pos;
    unsigned lostTurn;

public:
    Figure();
    virtual ~Figure();
    virtual void hide();
    virtual const QString toString() const;
    void setLost(unsigned turn) {lostTurn = turn;}
    unsigned getLostTurn() const {return lostTurn;}

    QQmlListProperty<Move> moves() const;

    Q_INVOKABLE void setMovedTimes(const unsigned value)
        { if(moved != value) movedTimesChanged(value); moved = value; }
    Q_INVOKABLE unsigned movedTimes() const {return moved;}
    Q_INVOKABLE void setPrimary(const bool value)   { if(isPrimary != value) primaryChanged(value); isPrimary = value; }
    Q_INVOKABLE bool primary() const {return isPrimary;}

    Q_INVOKABLE void setPosition(const QString &position)
        { if(position != pos) positionChanged(position); pos = position; }
    Q_INVOKABLE QString position() const {return pos;}

    Positions getAvailableMoves() const;
    bool canMove(const Item *item);
    void oppositeMoves();
    void rightMoves();
    void leftMoves();

signals:
    void movedTimesChanged(const unsigned move);
    void primaryChanged(const bool primary);
    void positionChanged(const QString &position);
    bool moveAllowed(Figure *sender, const unsigned x, const unsigned y);

public slots:
    void onSelect();
    void onUnselect();

    void onMove();
};
#endif // GAMEFIGURE_H
