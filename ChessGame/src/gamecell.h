#ifndef GAMECELL_H
#define GAMECELL_H

#include <QObject>
#include <gameitem.h>
#include <gamearmy.h>

class Cell : public Item
{
    Q_OBJECT
    Q_PROPERTY(unsigned num READ num WRITE setNum NOTIFY numChanged)
    unsigned number;
    Army::ArmySide occupied;
public:
    Cell();
    virtual ~Cell();
    virtual const QString toString() const;

    inline unsigned num() const {return number;}
    inline void setNum(unsigned num) {if(num != number) numChanged(); number = num;}

    inline Army::ArmySide occupiedBy() const {return occupied;}
    inline void setOccupied(Army::ArmySide value) {occupied = value;}

    void place(Item *item);

signals:
    void numChanged();
    void highlightAvailable();
    void highlightUnavailable();
    void stopHighlight();

public slots:
    void onSelect();
};
// this bad, but i dunno how to overcome it
typedef QVector<Cell *> Cells;
Cells getCells();
#endif // GAMECELL_H
