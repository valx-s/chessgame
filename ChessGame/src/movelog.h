#ifndef MOVELOG_H
#define MOVELOG_H
#include <QString>
#include <QStringList>

class MovesLog
{
    static const QString SEPARATOR;

    QString fileName;
    QStringList moves;
public:
    MovesLog(const QString &file = "save.gam");

    inline void setFile(const QString &file) {fileName = file;}
    inline QString getFile() const {return fileName;}

    bool save();
    bool load();

    void clear() {moves.clear();}
    unsigned movesCount() const;
    bool move(unsigned turn, QString &from, QString &to) const;
    void appendMove(const QString &from, const QString &to);
};

#endif // MOVELOG_H
