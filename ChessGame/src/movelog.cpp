#include "movelog.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
const QString MovesLog::SEPARATOR = ";";
//----------------------------------------------------------
MovesLog::MovesLog(const QString &file) : fileName(file)
{

}
//----------------------------------------------------------
bool MovesLog::save()
{
    QFile file(fileName);
    if(file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&file);
        for(int i = 0; i < moves.size(); ++i)
        {
            out << moves[i] << "\r";
//            qDebug() << moves[i];
        }
        return true;
    }
    qDebug() << "can't load file" << fileName;
    return false;
}

//--------------------------------------------------------
bool MovesLog::load()
{
    QFile file(fileName);
    if(file.open(QFile::ReadOnly))
    {
        QTextStream in(&file);
        while(!in.atEnd())
        {
            QString line;
            in >> line;
            if(line.isEmpty())
                break;
            moves.append(line);
//            qDebug() << line;
        }
        return true;
    }
    qDebug() << "can't load file" << fileName;
    return false;
}

//--------------------------------------------------------
unsigned MovesLog::movesCount() const
{
    return moves.size();
}
//--------------------------------------------------------
bool MovesLog::move(unsigned turn, QString &from, QString &to) const
{
    if(turn < movesCount())
    {
        const QStringList list = moves.at(turn).split(SEPARATOR);
        if(list.size() > 1)
        {
            from = list[0];
            to = list[1];
            return true;
        }
    }
    return false;
}
//--------------------------------------------------------
void MovesLog::appendMove(const QString &from, const QString &to)
{
    moves.append(QString("%1%2%3").arg(from, SEPARATOR, to));
}

//--------------------------------------------------------
