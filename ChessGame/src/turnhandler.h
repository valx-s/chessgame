#ifndef TURNHANDLER_H
#define TURNHANDLER_H

#include <QObject>
#include <QList>
#include <gamearmy.h>
//----------------------------------------------------------
enum TurnDirection {
    FORWARD, BACKWARD
};
//----------------------------------------------------------
//template <typename T>
//class sideMarker {
//        const T value;
//    public:
//        sideMarker(const T &mark) : value(mark) {}
//        const T &getValue() const {return value;}
//};
//typedef sideMarker<Army::ArmySide> Side;
typedef Army::ArmySide Side;
typedef QList<Side> Sides;
//----------------------------------------------------------
class TurnHandler : public QObject
{
    Q_OBJECT

    Sides sides;
    unsigned turn;
    TurnDirection direction;
    Side getNextSide() const;
public:
    explicit TurnHandler(QObject *parent = 0);
    virtual ~TurnHandler() {}
    void addSide(const Side &side) {sides.append(side);}

    void setDirection(TurnDirection value);
    TurnDirection getDirection() const {return direction;}
    unsigned getTurn() const {return turn;}

    void reset() {turn = 0; direction = FORWARD;}
    void nextTurn();

signals:
    void startTurn(const Side &side);
};
//----------------------------------------------------------
#endif // TURNHANDLER_H
