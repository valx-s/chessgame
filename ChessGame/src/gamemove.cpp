#include "gamemove.h"
#include "gamecellhandler.h"
//----------------------------------------------------------
Move::Move(QObject *parent) : QObject(parent), direction(ANY), type(NORMAL), distance(1), minDistance(1), allowed(true)
{

}
//----------------------------------------------------------
QString Move::toString() const
{
    return QString("%1 %2 %3-%4 %5").arg(::toString(direction), ::toString(type)).arg(minDistance).arg(distance).arg(allowed);
}
//----------------------------------------------------------
const QString toString(Move::MoveDirection value)
{
    switch(value)
    {
#define STR_TYPE(s) case Move::s: return #s;
    STR_TYPE(ANY) STR_TYPE(NORTH) STR_TYPE(SOUTH) STR_TYPE(WEST) STR_TYPE(EAST)
    STR_TYPE(NORTHWEST) STR_TYPE(NORTHEAST) STR_TYPE(SOUTHWEST) STR_TYPE(SOUTHEAST)
#undef STR_TYPE
    default:
        return QString("unknown %1").arg((int)value);
    }
}

//----------------------------------------------------------
const QString toString(Move::MoveType value)
{
    switch(value)
    {
#define STR_TYPE(s) case Move::s: return #s;
    STR_TYPE(NORMAL) STR_TYPE(FLY) STR_TYPE(ENEMY) STR_TYPE(FREE)
#undef STR_TYPE
    default:
        return QString("unknown %1").arg((int)value);
    }
}
//----------------------------------------------------------
Move::MoveDirection getOpposite(const Move::MoveDirection value)
{
    switch(value)
    {
    case Move::NORTH:
    default:
        return Move::SOUTH;
    case Move::SOUTH: return Move::NORTH;
    case Move::WEST: return Move::EAST;
    case Move::EAST: return Move::WEST;
    case Move::NORTHWEST: return Move::SOUTHEAST;
    case Move::NORTHEAST: return Move::SOUTHWEST;
    case Move::SOUTHWEST: return Move::NORTHEAST;
    case Move::SOUTHEAST: return Move::NORTHWEST;
    case Move::ANY: return Move::ANY;
    }
}
//----------------------------------------------------------
Move::MoveDirection getRight(const Move::MoveDirection value)
{
    switch(value)
    {
    case Move::NORTH:
    default:
        return Move::WEST;
    case Move::SOUTH: return Move::EAST;
    case Move::WEST: return Move::NORTH;
    case Move::EAST: return Move::SOUTH;
    case Move::NORTHWEST: return Move::SOUTHWEST;
    case Move::NORTHEAST: return Move::NORTHWEST;
    case Move::SOUTHWEST: return Move::SOUTHEAST;
    case Move::SOUTHEAST: return Move::NORTHEAST;
    case Move::ANY: return Move::ANY;
    }
}
//----------------------------------------------------------
Move::MoveDirection getLeft(const Move::MoveDirection value)
{
    return getOpposite(getRight(value));
}
//----------------------------------------------------------
bool checkMove(Positions &movement, Move::MoveType type, Army::ArmySide side, const Pos &move_pos)
{
    bool canContinue = true;
    Cell *moveCell = CellsUtil.getCell(move_pos);
    if(moveCell)
    {
        switch(type)
        {
        case Move::NORMAL:
            if(moveCell->occupiedBy() != side)
                movement.push_back(move_pos);
            canContinue = moveCell->occupiedBy() == Army::NONE;
            break;
        case Move::FLY:
            if(moveCell->occupiedBy() != side)
                movement.push_back(move_pos);
            break;
        case Move::ENEMY:
            canContinue = moveCell->occupiedBy() != Army::NONE && moveCell->occupiedBy() != side;
            if(canContinue)
                movement.push_back(move_pos);
            break;
        case Move::FREE:
            canContinue = moveCell->occupiedBy() == Army::NONE;
            if(canContinue)
                movement.push_back(move_pos);
            break;
        }
    }
    else
        canContinue = false;
    return canContinue;
}

//----------------------------------------------------------
Positions Move::moves(const Pos &pos) const
{
    const unsigned minPos = 1;
    const Army::ArmySide side = CellsUtil.getCell(pos)->occupiedBy();
    bool canContinue = true;
    // direction, type, distance
    Positions movement;
    for(unsigned i = minDistance; i <= distance && canContinue; i++)
    {
        Pos move_pos = pos;
        switch(direction)
        {
        case ANY:
            for(unsigned x = pos.x > i ? pos.x - i : minPos; x <= pos.x + i; ++x)
            {
                move_pos.x = x;
                move_pos.y = pos.y - i;
                checkMove(movement, type, side, move_pos);
                move_pos.y = pos.y + i;
                checkMove(movement, type, side, move_pos);
            }
            for(unsigned y = pos.y > i ? pos.y - i : minPos; y <= pos.y + i; ++y)
            {
                move_pos.x = pos.x - i;
                move_pos.y = y;
                checkMove(movement, type, side, move_pos);
                move_pos.x = pos.x + i;
                checkMove(movement, type, side, move_pos);
            }
            continue;
        case NORTH:
            move_pos.y += i;
            break;
        case SOUTH:
            move_pos.y -= i;
            break;
        case WEST:
            move_pos.x += i;
            break;
        case EAST:
            move_pos.x -= i;
            break;
        case NORTHWEST:
            move_pos.y += i;
            move_pos.x += i;
            break;
        case NORTHEAST:
            move_pos.y += i;
            move_pos.x -= i;
            break;
        case SOUTHWEST:
            move_pos.y -= i;
            move_pos.x += i;
            break;
        case SOUTHEAST:
            move_pos.y -= i;
            move_pos.x -= i;
            break;
        }
        canContinue = checkMove(movement, type, side, move_pos);
    }
    return movement;
}
//----------------------------------------------------------
