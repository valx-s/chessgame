#include "turnhandler.h"
#include <QDebug>
//----------------------------------------------------------
TurnHandler::TurnHandler(QObject *parent) : QObject(parent), turn(0), direction(FORWARD)
{

}
//----------------------------------------------------------
Side TurnHandler::getNextSide() const
{
    return sides.count() && turn ? sides[(turn - 1) % sides.count()] : Side(Army::NONE);
}
//----------------------------------------------------------
void TurnHandler::setDirection(TurnDirection value)
{
    if(direction != value)
        direction = value;
}
//----------------------------------------------------------
void TurnHandler::nextTurn()
{
    switch(direction)
    {
    case FORWARD:
        turn++;
        emit startTurn(getNextSide());
        break;
    case BACKWARD:
        if(turn > 1)
        {
            turn--;
            emit startTurn(getNextSide());
        }
        break;
    }
//    qDebug() << __FUNCTION__ << turn;
}
//----------------------------------------------------------
