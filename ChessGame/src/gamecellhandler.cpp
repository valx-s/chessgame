#include "gamecellhandler.h"
#include <QtMath>
CellsHandler CellsUtil;
//----------------------------------------------------------
CellsHandler::CellsHandler() : cols(0), rows(0)
{

}
//----------------------------------------------------------
void CellsHandler::prepareCells(unsigned colCount, unsigned rowCount)
{
    cols = colCount;
    rows = rowCount;
    for(Cells::iterator cell = cells.begin(); cell != cells.end(); ++cell)
    {
        Cell *c = *cell;
        c->setPos(c->num() % rows + 1, c->num() / rows + 1);
        c->setOccupied(Army::NONE);
    }
}

//----------------------------------------------------------
Cell *CellsHandler::getCell(unsigned x, unsigned y)
{
    for(Cells::iterator cell = cells.begin(); cell != cells.end(); ++cell)
        if((*cell)->at(x, y))
            return *cell;
    return NULL;
}

//----------------------------------------------------------
Cell *CellsHandler::getCell(const QString &pos)
{
    for(Cells::iterator cell = cells.begin(); cell != cells.end(); ++cell)
        if((*cell)->at(pos))
            return *cell;
    return NULL;
}
//----------------------------------------------------------
Cell *CellsHandler::getCellByCoords(unsigned x, unsigned y, unsigned x_range, unsigned y_range)
{
    Cells::iterator i = cells.begin();

    Cell *cell = *i;
    qreal min_dist = qFabs(cell->distance(x, y, x_range, y_range));
    for(; i != cells.end(); ++i)
    {
        qreal dist = qFabs((*i)->distance(x, y, x_range, y_range));
        if(min_dist > dist)
        {
            min_dist = dist;
            cell = *i;
        }
    }
    return cell;
}
//----------------------------------------------------------
void CellsHandler::highlightAvailable(const Positions &list)
{
    for(Cells::iterator i = cells.begin(); i != cells.end(); ++i)
        for(Positions::ConstIterator j = list.begin(); j != list.end(); ++j)
        {
            if((*i)->at(*j))
                (*i)->highlightAvailable();
        }
}

//----------------------------------------------------------
void CellsHandler::highlightUnavailable(const Positions &list)
{
    for(Cells::iterator i = cells.begin(); i != cells.end(); ++i)
        for(Positions::ConstIterator j = list.begin(); j != list.end(); ++j)
        {
            if((*i)->at(*j))
                (*i)->highlightUnavailable();
        }
}
//----------------------------------------------------------
void CellsHandler::stopHighlight()
{
    for(Cells::iterator i = cells.begin(); i != cells.end(); ++i)
        (*i)->stopHighlight();
}

//----------------------------------------------------------
