#ifndef GAMEARMY_H
#define GAMEARMY_H

#include <QObject>
#include <QQuickItem>
#include <gamefigure.h>
#include <gamemove.h>

typedef QList<Figure *> ArmyFigures;

class Army : public QObject
{
public:
    enum ArmySide {
        NONE, WHITE, BLACK
    };
    enum ArmyDirection {
        NORTH, SOUTH, WEST, EAST
    };

private:
    Q_OBJECT
    Q_ENUMS(ArmySide)
    Q_PROPERTY(ArmySide side READ getSide WRITE setSide)
    ArmySide side;

    Q_ENUMS(ArmyDirection)
    Q_PROPERTY(ArmyDirection direction READ getDirection WRITE setDirection)
    ArmyDirection direction;
    Q_PROPERTY(ArmyDirection defaultDirection READ getDefaultDirection WRITE setDefaultDirection)
    ArmyDirection defDirection;

    Q_PROPERTY(QQmlListProperty<Figure> figures READ figures)
    static void append_figure(QQmlListProperty<Figure> *list, Figure *item);
    ArmyFigures m_figures;

    Q_PROPERTY(QString position READ getPosition WRITE setPosition)
    QString pos;

    bool adjusted;
public:
    explicit Army(QObject *parent = 0);
    virtual ~Army() {}

    virtual QString toString() const;

    Q_INVOKABLE void setSide(ArmySide value) {side = value;}
    Q_INVOKABLE ArmySide getSide() const {return side;}
    Q_INVOKABLE void setDirection(ArmyDirection value) {direction = value;}
    Q_INVOKABLE ArmyDirection getDirection() const {return direction;}
    Q_INVOKABLE void setDefaultDirection(ArmyDirection value) {defDirection = value;}
    Q_INVOKABLE ArmyDirection getDefaultDirection() const {return defDirection;}
    Q_INVOKABLE void setPosition(const QString &value) {pos = value;}
    Q_INVOKABLE QString getPosition() const {return pos;}

    QQmlListProperty<Figure> figures() const;
    ArmyFigures getArmyFigures() {return m_figures;}

    void placeFigures();
    void enableFigures(bool enable);
    Figure *getFigureAt(const Item *position);
    void lostFigure(const Item *figure, const unsigned turn);
    void resurectFigure(const unsigned turn);

signals:
    void error(QString msg);
    void warning(QString msg);

public slots:
};
const QString toString(Army::ArmySide value);
const QString toString(Army::ArmyDirection value);

Army::ArmyDirection getOpposite(const Army::ArmyDirection value);
Army::ArmyDirection getRight(const Army::ArmyDirection value);
Army::ArmyDirection getLeft(const Army::ArmyDirection value);
#endif // GAMEARMY_H
