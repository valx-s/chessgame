#ifndef GAMEITEM_H
#define GAMEITEM_H

#include <QObject>
#include <QQuickItem>
#include <QSize>
#include <QVector>

struct Pos {
    unsigned x; // column
    unsigned y; // row
    Pos() : x(0), y(0) {}
    Pos(const QString &pos);
    inline bool operator ==(const Pos &other) const
        { return x == other.x && y == other.y;}
    inline bool valid() const {return x && y;}
    QString asString() const;
};
typedef QVector<Pos> Positions;

class Item : public QQuickItem
{
    Q_OBJECT
    Pos position;
public:
    Item();
    virtual ~Item();
    virtual const QString toString() const;

    void setPos(unsigned x, unsigned y) {position.x = x; position.y = y;}
    void setPos(const QString &pos);

    const Pos getPos() const {return position;}
    bool at(unsigned x, unsigned y) const;
    bool at(const Pos &pos) const { return at(pos.x, pos.y); }
    bool at(const QString &pos) const;
    qreal distance(unsigned x, unsigned y, unsigned x_range, unsigned y_range);

    qreal getTop() const   { return y(); }
    qreal getLeft() const  { return x(); }
    void setTop(qreal value)   { setY(value); }
    void setLeft(qreal value)  { setX(value); }

    virtual void show() { setVisible(true); }
    virtual void hide() { setVisible(false); }

signals:

public slots:
};
#endif // GAMEITEM_H
