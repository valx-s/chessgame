#ifndef GAMEMOVE_H
#define GAMEMOVE_H

#include <QObject>
#include <QQuickItem>
#include <gameitem.h>

class Move : public QObject
{
public:
    enum MoveDirection {
        ANY, NORTH, SOUTH, WEST, EAST, NORTHWEST, NORTHEAST, SOUTHWEST, SOUTHEAST
    };
    enum MoveType {
        NORMAL, FLY, ENEMY, FREE
    };

private:
    Q_OBJECT
    Q_ENUMS(MoveDirection)
    Q_PROPERTY(MoveDirection direction READ getDirection WRITE setDirection)
    MoveDirection direction;

    Q_ENUMS(MoveType)
    Q_PROPERTY(MoveType type READ getType WRITE setType)
    MoveType type;

    Q_PROPERTY(unsigned distance READ getDistance WRITE setDistance)
    unsigned distance;
    Q_PROPERTY(unsigned minDistance READ getMinDistance WRITE setMinDistance)
    unsigned minDistance;

    Q_PROPERTY(bool allowed READ getAllowed WRITE setAllowed NOTIFY allowedChanged)
    bool allowed;
public:
    explicit Move(QObject *parent = 0);
    virtual ~Move() {}

    virtual QString toString() const;

    Q_INVOKABLE void setDirection(MoveDirection value) {direction = value;}
    Q_INVOKABLE MoveDirection getDirection() const {return direction;}
    Q_INVOKABLE void setType(MoveType value) {type = value;}
    Q_INVOKABLE MoveType getType() const {return type;}

    Q_INVOKABLE void setDistance(unsigned value) {distance = value;}
    Q_INVOKABLE unsigned getDistance() const {return distance;}
    Q_INVOKABLE void setMinDistance(unsigned value) {minDistance = value;}
    Q_INVOKABLE unsigned getMinDistance() const {return minDistance;}
    Q_INVOKABLE void setAllowed(bool value) {if(allowed != value) allowedChanged(value); allowed = value;}
    Q_INVOKABLE bool getAllowed() const {return allowed;}

    Positions moves(const Pos &pos) const;

signals:
    void allowedChanged(bool value);

public slots:
};
const QString toString(Move::MoveDirection value);
const QString toString(Move::MoveType value);

Move::MoveDirection getOpposite(const Move::MoveDirection value);
Move::MoveDirection getRight(const Move::MoveDirection value);
Move::MoveDirection getLeft(const Move::MoveDirection value);
#endif // GAMEMOVE_H
