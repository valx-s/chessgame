#ifndef GAMECELLHANDLER_H
#define GAMECELLHANDLER_H
#include <QObject>
#include <gameitem.h>
#include <gamecell.h>

class CellsHandler
{
    unsigned cols;
    unsigned rows;
    Cells cells;
public:
    CellsHandler();

    void setCells(const Cells &value) { cells = value; }

    void prepareCells(unsigned cols, unsigned rows);
    Cell *getCell(const Pos &pos) {return getCell(pos.x, pos.y);}
    Cell *getCell(unsigned x, unsigned y);
    Cell *getCell(const QString &pos);
    Cell *getCellByCoords(unsigned x, unsigned y, unsigned x_range, unsigned y_range);

    unsigned getCols() const {return cols;}
    unsigned getRows() const {return rows;}

    void highlightAvailable(const Positions &list);
    void highlightUnavailable(const Positions &list);
    void stopHighlight();

};
// sorry =)
extern CellsHandler CellsUtil;

#endif // GAMECELLHANDLER_H
