#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>
#include <QQuickItem>
#include <QQmlListProperty>
#include <movelog.h>
#include <gamecell.h>
#include <gamearmy.h>
#include <turnhandler.h>

typedef QList<Army *> Armies;

class Game : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(unsigned rows READ rows WRITE setRows NOTIFY rowsChanged)
    unsigned rowCount;
    Q_PROPERTY(unsigned cols READ cols WRITE setCols NOTIFY colsChanged)
    unsigned colCount;

    Q_PROPERTY(QQmlListProperty<Army> armies READ armies)
    static void append_army(QQmlListProperty<Army> *list, Army *item);
    Armies m_armies;

    Q_PROPERTY(Army::ArmySide currentSide READ currentSide WRITE setCurrentSide NOTIFY currentSideChanged)
    Army::ArmySide curSide;

    Q_PROPERTY(unsigned turn READ getTurn)
    Q_PROPERTY(QString file READ getFile WRITE setFile NOTIFY fileChanged)

    Army *getArmy(Army::ArmySide side);
    MovesLog movesLog;
    TurnHandler turns;

    bool figureMove(Figure *sender, Cell *from, Cell *to);

    void checkPrimary();
public:
    explicit Game();
    virtual ~Game();

    Q_INVOKABLE unsigned rows() const {return rowCount;}
    Q_INVOKABLE void setRows(unsigned count) {if(count != rowCount) rowsChanged(); rowCount = count;}
    Q_INVOKABLE unsigned cols() const {return colCount;}
    Q_INVOKABLE void setCols(unsigned count) {if(count != colCount) colsChanged(); colCount = count;}

    QQmlListProperty<Army> armies() const;

    Q_INVOKABLE Army::ArmySide currentSide() const {return curSide;}
    Q_INVOKABLE void setCurrentSide(Army::ArmySide value);
    Q_INVOKABLE unsigned getTurn() const {return turns.getTurn();}

    Q_INVOKABLE QString getFile() const {return movesLog.getFile();}
    Q_INVOKABLE void setFile(const QString &value) {if(getFile() != value) fileChanged(); movesLog.setFile(value);}

signals:
    void rowsChanged();
    void colsChanged();

    void gameStarted();
    void gameStopped();

    void currentSideChanged();
    void fileChanged();

    void error(QString mess);
    void warning(QString mess);

public slots:
    void start();
    void stop();

    void load();
    void save();

    void prevLoadedMove();
    void nextLoadedMove();

    bool figureMove(Figure *sender, const unsigned x, const unsigned y);
    void onStartTurn(Side side) {setCurrentSide(side);}
};

#endif // GAMECONTROLLER_H
