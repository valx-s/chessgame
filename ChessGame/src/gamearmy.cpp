#include "gamearmy.h"
#include <gamecellhandler.h>
//----------------------------------------------------------
Army::Army(QObject *parent) : QObject(parent), side(WHITE),
    direction(NORTH), defDirection(NORTH), pos("a1"), adjusted(false)
{

}
//----------------------------------------------------------
QString Army::toString() const
{
    return QString("%1 %2 %3 %4").arg(::toString(side), ::toString(direction), pos).arg(m_figures.size());
}
//----------------------------------------------------------
QQmlListProperty<Figure> Army::figures() const
{
    return QQmlListProperty<Figure>((QObject *)this, 0, &Army::append_figure, NULL, NULL, NULL);
}
//----------------------------------------------------------
enum AdjustDirection {
    SAME, OPPOSITE, RIGHT, LEFT
};

AdjustDirection adjustDirection(const Army::ArmyDirection direction, const Army::ArmyDirection defDirection)
{
    if(direction == getOpposite(defDirection))
        return OPPOSITE;
    else if(direction == getRight(defDirection))
        return RIGHT;
    else if(direction == getLeft(defDirection))
        return LEFT;
     else
        return SAME;
}
//----------------------------------------------------------
void Army::append_figure(QQmlListProperty<Figure> *list, Figure *item)
{
    Army *parent = qobject_cast<Army *>(list->object);
    if (parent)
    {
        parent->m_figures.append(item);
    }
}
//----------------------------------------------------------
const QString toString(Army::ArmySide value)
{
    switch(value)
    {
#define STR_TYPE(s) case Army::s: return #s;
    STR_TYPE(NONE) STR_TYPE(WHITE) STR_TYPE(BLACK)
#undef STR_TYPE
    default:
        return QString("unknown %1").arg((int)value);
    }
}
//----------------------------------------------------------
const QString toString(Army::ArmyDirection value)
{
    switch(value)
    {
#define STR_TYPE(s) case Army::s: return #s;
    STR_TYPE(NORTH) STR_TYPE(SOUTH) STR_TYPE(WEST) STR_TYPE(EAST)
#undef STR_TYPE
    default:
        return QString("unknown %1").arg((int)value);
    }
}
//----------------------------------------------------------
void incPos(Pos &pos, Army::ArmyDirection direction)
{
    switch(direction)
    {
    case Army::NORTH:
        pos.x = (pos.x + 1) % (CellsUtil.getCols() + 1);
        if(!pos.x && pos.y)
        {
            pos.y--;
            pos.x++;
        }
        break;
    case Army::SOUTH:
        pos.x = (pos.x + 1) % (CellsUtil.getCols() + 1);
        if(!pos.x)
        {
            pos.y = (pos.y + 1) % (CellsUtil.getRows() + 1);
            pos.x++;
        }
        break;
    case Army::WEST:
        pos.y = (pos.y + 1) % (CellsUtil.getRows() + 1);
        if(!pos.y)
        {
            pos.x = (pos.x + 1) % (CellsUtil.getCols() + 1);
            pos.y++;
        }
        break;
    case Army::EAST:
        pos.y = (pos.y + 1) % (CellsUtil.getRows() + 1);
        if(!pos.y && pos.x)
        {
            pos.x--;
            pos.y++;
        }
        break;
    }
}
//----------------------------------------------------------
void Army::placeFigures()
{
    enableFigures(false);

    Pos initPos(pos);
    for(ArmyFigures::Iterator i = m_figures.begin(); i != m_figures.end(); ++i)
    {
        Figure *figure = *i;
        const Pos figurePos(figure->position());
        if(figurePos.valid())
            initPos = figurePos;
        else if(!initPos.valid())
        {
            qDebug() << "failed to place figure !";
            break;
        }
        Cell *cell = CellsUtil.getCell(initPos.x, initPos.y);
        if(cell)
        {
            cell->place(figure);
            cell->setOccupied(getSide());
            figure->setLost(0);
            figure->setMovedTimes(0);
        }
        else
        {
            qDebug() << "cell not found at" << initPos.x << initPos.y;
            break;
        }

        if(!adjusted)
        {
            switch(adjustDirection(direction, defDirection))
            {
            case OPPOSITE: figure->oppositeMoves();
                break;
            case RIGHT: figure->rightMoves();
                break;
            case LEFT: figure->leftMoves();
                break;
            default: break;
            }
        }

        incPos(initPos, direction);
    }
    adjusted = true;
}

//----------------------------------------------------------
void Army::enableFigures(bool enable)
{
    for(ArmyFigures::Iterator i = m_figures.begin(); i != m_figures.end(); ++i)
    {
        Figure *figure = *i;
        figure->setEnabled(enable);
    }
}
//----------------------------------------------------------
Figure *Army::getFigureAt(const Item *item)
{
    for(ArmyFigures::Iterator i = m_figures.begin(); i != m_figures.end(); ++i)
        if((*i)->getPos() == item->getPos() && (*i)->isVisible())
            return *i;
    return NULL;
}

//----------------------------------------------------------
void Army::lostFigure(const Item *item, const unsigned turn)
{
    Figure *figure = getFigureAt(item);
    if(figure)
    {
        qDebug() << toString() << "lost" << figure->toString();
        figure->setLost(turn);
        figure->hide();

        if(figure->primary())
            warning(::toString(side) + "\nLost primary !");
    }
}
//----------------------------------------------------------
void Army::resurectFigure(const unsigned turn)
{
    for(ArmyFigures::Iterator i = m_figures.begin(); i != m_figures.end(); ++i)
        if((*i)->getLostTurn() == turn)
        {
            qDebug() << toString() << "resurected" << (*i)->toString();
            Cell *cell = CellsUtil.getCell((*i)->getPos());
            if(cell)
            {
                cell->place(*i);
                cell->setOccupied(side);
            }
        }
}

//----------------------------------------------------------
Army::ArmyDirection getOpposite(const Army::ArmyDirection value)
{
    switch(value)
    {
    case Army::NORTH:
    default:
        return Army::SOUTH;
    case Army::SOUTH: return Army::NORTH;
    case Army::WEST: return Army::EAST;
    case Army::EAST: return Army::WEST;
    }
}
//----------------------------------------------------------
Army::ArmyDirection getRight(const Army::ArmyDirection value)
{
    switch(value)
    {
    case Army::NORTH:
    default:
        return Army::WEST;
    case Army::SOUTH: return Army::EAST;
    case Army::WEST: return Army::NORTH;
    case Army::EAST: return Army::SOUTH;
    }
}
//----------------------------------------------------------
Army::ArmyDirection getLeft(const Army::ArmyDirection value)
{
    return getOpposite(getRight(value));
}
//----------------------------------------------------------
