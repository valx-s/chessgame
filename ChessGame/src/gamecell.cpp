#include "gamecell.h"
Cells loadedCells;
//----------------------------------------------------------
Cells getCells() {return loadedCells;}
//----------------------------------------------------------
Cell::Cell() : Item(), number(0), occupied(Army::NONE)
{
    loadedCells.push_back(this);
}
//----------------------------------------------------------
Cell::~Cell()
{
    loadedCells.remove(loadedCells.indexOf(this));
}
//----------------------------------------------------------
const QString Cell::toString() const
{
    return QString("%1 %2 %3").arg(Item::toString()).arg(number).arg(::toString(occupied));
}
//----------------------------------------------------------
void Cell::onSelect()
{
    qDebug() << __FUNCTION__ << toString();
}
//----------------------------------------------------------
void Cell::place(Item *item)
{
    item->setTop(getTop());
    item->setLeft(getLeft());

    item->setPos(getPos().x, getPos().y);

    item->show();
}

//----------------------------------------------------------
