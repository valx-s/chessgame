import QtQuick 2.0

Rectangle {
    id: menuRect
    width: parent.width
    color: "black"

    property alias menuBtn1: btn1
    property alias menuBtn2: btn2
    property alias menuBtn3: btn3
    property alias menuBtn4: btn4

    Row {

        width: parent.width
        spacing: 10

        MenuButton {
            id: btn1
            height: menuRect.height
            caption: "start"
        }
        MenuButton {
            id: btn2
            height: menuRect.height
            caption: "load"
        }
        MenuButton {
            id: btn3
            height: menuRect.height
            caption: "settings"
        }
        MenuButton {
            id: btn4
            height: menuRect.height
            caption: "close"
            onButtonClicked: Qt.quit()
        }

    }
}

