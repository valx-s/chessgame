import QtQuick 2.0

Rectangle {
    width: 100
    height: 50;

    property alias caption: label.text
    Text {
        id: label
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    property color bColor: "lightsteelblue"
    gradient: Gradient {
        GradientStop { position: 0.0; color: mouseArea.pressed ? Qt.darker(bColor, 1.5) : bColor }
        GradientStop { position: 0.7; color: mouseArea.pressed ? Qt.darker(bColor, 1.5) : bColor }
        GradientStop { position: 1.0; color: "slategray" }
    }

    signal buttonClicked()

    property color borderColor: "yellow"
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        onEntered: parent.border.color = Qt.darker(borderColor, 1.5)
        onExited: parent.border.color = borderColor
        onClicked: buttonClicked()
    }

    radius: 10
    border {
        width: 2
        color: borderColor
    }
}
