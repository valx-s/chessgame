import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2

ApplicationWindow {
    title: qsTr("Chess game")
    width: 500
    height: 620
    visible: true

    MainWindow {
        anchors.fill: parent
    }
}
