import QtQuick 2.0
import Controller 1.0

Army {
    side: Army.WHITE
    position: "a1"
    defaultDirection: Army.SOUTH

    property alias parent: figure.parent
    property string fraction: ""
    property alias size: figure.width

    figures: [
        BoardFigure { id: figure; image: fraction+"rook";
            moves: [
                Move { direction: Move.WEST; distance: 8; type: Move.NORMAL },
                Move { direction: Move.EAST; distance: 8; type: Move.NORMAL },
                Move { direction: Move.SOUTH; distance: 8; type: Move.NORMAL },
                Move { direction: Move.NORTH; distance: 8; type: Move.NORMAL }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"horse";
            moves: [
                Move { direction: Move.ANY; minDistance: 2; distance: 2; type: Move.FLY },
                Move { direction: Move.SOUTHWEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.SOUTHEAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTHWEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTHEAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.WEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.EAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.SOUTH; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTH; distance: 2; type: Move.FLY; allowed: false }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"bishop";
            moves: [
                Move { direction: Move.SOUTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.SOUTHEAST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHEAST; distance: 8; type: Move.NORMAL; }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"king"; primary: true;
            moves: [
                Move { direction: Move.ANY; distance: 1; type: Move.NORMAL }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"queen";
            moves: [
                Move { direction: Move.SOUTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.SOUTHEAST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHEAST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.WEST; distance: 8; type: Move.NORMAL;  },
                Move { direction: Move.EAST; distance: 8; type: Move.NORMAL;  },
                Move { direction: Move.SOUTH; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTH; distance: 8; type: Move.NORMAL; }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"bishop";
            moves: [
                Move { direction: Move.SOUTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.SOUTHEAST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHWEST; distance: 8; type: Move.NORMAL; },
                Move { direction: Move.NORTHEAST; distance: 8; type: Move.NORMAL; }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"horse";
            moves: [
                Move { direction: Move.ANY; minDistance: 2; distance: 2; type: Move.FLY },
                Move { direction: Move.SOUTHWEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.SOUTHEAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTHWEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTHEAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.WEST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.EAST; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.SOUTH; distance: 2; type: Move.FLY; allowed: false },
                Move { direction: Move.NORTH; distance: 2; type: Move.FLY; allowed: false }
            ] },
        BoardFigure { parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;
            image: fraction+"rook";
            moves: [
                Move { direction: Move.WEST; distance: 8; type: Move.NORMAL },
                Move { direction: Move.EAST; distance: 8; type: Move.NORMAL },
                Move { direction: Move.SOUTH; distance: 8; type: Move.NORMAL },
                Move { direction: Move.NORTH; distance: 8; type: Move.NORMAL }
            ] },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" },
        BoardPawn { parent: figure.parent; width: figure.width; image: fraction+"pawn" }
    ]
}
