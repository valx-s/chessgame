import QtQuick 2.0
import Controller 1.0

BoardFigure { /*parent: figure.parent; width: figure.width; overlayColor: figure.overlayColor;*/
    image: "figures/wpawn";
    onMovedTimesChanged:  { first.allowed = (move === 0) ? true : false; }

    moves: [
        Move { id: first; direction: Move.NORTH; distance: 2; type: Move.FREE; },
        Move { direction: Move.NORTH; distance: 1; type: Move.FREE; },
        Move { direction: Move.NORTHWEST; distance: 1; type: Move.ENEMY; },
        Move { direction: Move.NORTHEAST; distance: 1; type: Move.ENEMY; }
    ] }
