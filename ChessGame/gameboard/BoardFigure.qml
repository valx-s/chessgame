import QtQuick 2.0
import QtGraphicalEffects 1.0
import Controller 1.0

Figure {
    property alias image: img.source
    property alias overlayColor: overlay.color
    width: 50
    height: width

    Image {
        id:img
        width: parent.width
        height: parent.height

        smooth: true
        z:10

        fillMode: Image.PreserveAspectFit
    }

    ColorOverlay {
        id: overlay
        anchors.fill: img
        source: img
        color: "#00000000"
    }

    MouseArea {
        anchors.fill: parent
        enabled: true
        hoverEnabled: true

        drag.target: parent
        drag.axis: Drag.None

        onPressed: {
            drag.axis = Drag.XAndYAxis
        }

        onReleased: {
            drag.axis = Drag.None
            parent.onMove()
        }

        onEntered: {
            parent.onSelect()
        }
        onExited: {
            parent.onUnselect()
        }
    }
}

