import QtQuick 2.0
import QtQuick.Dialogs 1.2
import Controller 1.0

Game {
    id: game
    cols: rows
    property alias dimensions: game.rows
    property alias color: board.color
    property alias enabled: board.enabled
    property alias borders: grid.x
    property int size: 60

    onCurrentSideChanged: {
        fillTurnLabel();
    }
    onGameStarted: {
        fillTurnLabel();
        turnMenu.visible = true;
    }
    onGameStopped: {
        turnMenu.visible = false;
    }

    onWarning: {
        msgDialog.title = qsTr("Warning")
        msgDialog.show(mess)
    }

    onError: {
        msgDialog.title = qsTr("Error")
        msgDialog.show(mess)
    }

    function fillTurnLabel() {
        switch(game.currentSide)
        {
        case Army.WHITE: caption.text = "White player. Turn: "; break;
        case Army.BLACK: caption.text = "Black player. Turn: "; break;
        }
        caption.text += game.turn
    }

    Column {
        parent: game.parent
        width: parent.width

        spacing: 10
    Rectangle {
        id: turnMenu
        visible: false
        color: "grey"
        height: 30
        width: parent.width

        Text {
            id: caption
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

    }

    Rectangle {
        id: board
        visible: game.visible

        color: "black"

        width: dimensions * game.size + borders * 2 + grid.spacing * grid.rows
        height: width

        Grid {

            id: grid
            x: game.borders; y: x
            rows: game.rows; columns: game.cols;
            spacing: 1

            Repeater {
                model: grid.rows * grid.columns
                delegate: boardCell
            }
            Component {
                id: boardCell
    //game.cells: [
                BoardGlow {
                    width: game.size
                    height: game.size

                    num: index
                    color: ((index + Math.floor(index / grid.rows) % 2) % 2) ? "grey" : "white"
                    glowColor: "blue"
                }
    //]
            }
        }

    }
    }
    armies: [
        BoardArmy { parent: grid; size: game.size; side: Army.WHITE; direction: Army.SOUTH; position: "a1"; fraction: "/figures/w" },
        BoardArmy { parent: grid; size: game.size; side: Army.BLACK; direction: Army.NORTH; position: "a8"; fraction: "/figures/b" }
    ]

    MessageDialog {
        id: msgDialog

        function show(caption) {
            msgDialog.text = caption;
            msgDialog.open();
        }
    }
}
