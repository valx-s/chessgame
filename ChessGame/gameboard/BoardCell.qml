import QtQuick 2.0
import Controller 1.0

Cell {
    id: cell
    property alias color: rect.color

    onHighlightAvailable: {
        //console.log("highlighted ! " + txt.text)
    }
    onHighlightUnavailable: {
        console.log("highlighted unavailable ! " + txt.text)
    }

    Rectangle {
        id: rect
        width: parent.width
        height: width

        color: "green"
        z:10

        Text {
            id: txt
            text: cell.num
        }
    }
}
