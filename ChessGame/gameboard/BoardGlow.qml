import QtQuick 2.0
import QtGraphicalEffects 1.0

BoardCell {

    onHighlightAvailable: {
        state = "Glowing"
    }
    onHighlightUnavailable: {
        state = "Glowing"
    }

    onStopHighlight: {
        state = ""
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onEntered: parent.state = "Glowing"
        onExited: parent.state = ""

        onClicked: {
            parent.onSelect()
        }
    }

    property color glowColor: "red"
    RectangularGlow {
        id: effect
        anchors.fill: parent
        glowRadius: 10
        spread: 0.2
        color: glowColor
        visible: false
        cornerRadius: 25
        z: 100
    }
    Rectangle {
        id: bRect
        anchors.fill: parent
        color: parent.color
        visible: false
    }
    states: [
        State {
            name: "Glowing"

            PropertyChanges {
                target: effect
                visible: true
            }

//            PropertyChanges {
//                target: bRect
//                visible: true
//            }
        }
    ]
}

