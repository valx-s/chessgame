TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    src/gamearmy.cpp \
    src/gamecell.cpp \
    src/gamecellhandler.cpp \
    src/gamecontroller.cpp \
    src/gamefigure.cpp \
    src/gameitem.cpp \
    src/gamemove.cpp \
    src/movelog.cpp \
    src/turnhandler.cpp

RESOURCES += qml.qrc

INCLUDEPATH += src/

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =src/ \
    gameboard/

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/gamearmy.h \
    src/gamecell.h \
    src/gamecellhandler.h \
    src/gamecontroller.h \
    src/gamefigure.h \
    src/gameitem.h \
    src/gamemove.h \
    src/movelog.h \
    src/turnhandler.h
