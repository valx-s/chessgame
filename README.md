QML ChessGame

Supports various figure types, orientation(limited), warring factions.

TODO:
- fix orientation bug
- add turn timer
- add random events
- add castling ("dependant" moves)
- check, checkmate